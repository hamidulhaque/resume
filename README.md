# RESUME 
## FLY ABOVE AND LAUGH

# Hamidul Haque Hamid, CSE Graduate

<div style="text-align: center;">
 "I am an adaptive learner and have a passion for technology. I am always eager to adopt and familiarize myself with new and emerging technologies."
</div>

## Education
- Bachelor of Science in Computer Science and Engineering, Uttara University

## Skills
- Mid level Laravel Developer
- Experience with web development using HTML, CSS, JavaScript, and PhP
- Familiarity with databases and MySQL

## Experience
- Executive at ARAM INTERNATIONAL
- Laravel Developer at PONDITS
- Computer Hardware TroubleShooter at ULTRA COMPUTERS

## Projects
- Online shopping website using LARAVEL 
- BLOOD DONATION MANAGEMENT SYSTEM USING LARAVEL
- Personal portfolio website using HTML, CSS, and JavaScript

## Achievements 
- Student Of the batch (PONDITS SEIP PHP-LARAVEL B4)
- DEAN AWARD 2021 (Obtaining 3.94 out of 4)
- DEAN AWARD 2020 (Obtaining 4.00 out of 4) 

## Trainings
- SEIP LARAVEL PHP
- MICROSOFT OFFICE APPLICATIONS

## Interests
- Computer Hardware 
- Web development
- Artificial Intelligence and Machine Learning (NOOB)


## Contact
- Email: hamidulhaque12@hotmail.com
- LinkedIn: [Hamidul Haque Hamid](https://www.linkedin.com/in/hamidulhaquehamid)
- GitLab: [Hamidul Haque Hamid](https://gitlab.com/hamidulhaque)
- Facebook: [Hamidul Haque Hamid](https://facebook.com/hamidulhaque13)
